package day1

import java.io.File

fun main(){

    val listOfIntegers = File("day1input.csv")
        .readLines()
        .map { it.toInt() }

    for (i in listOfIntegers){

        for (j in listOfIntegers) {
            val result = j + i
            if (result==2020){

                val product = j * i

                println("First number is $j")
                println("Second number is $i")
                println("Product is $product")

            }
        }


    }

}