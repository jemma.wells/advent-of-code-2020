package day1

import java.io.File

fun main() {

    val listOfIntegers = File("day1input.csv")
        .readLines()
        .map { it.toInt() }

    for (i in listOfIntegers) {

        for (j in listOfIntegers) {

            for (k in listOfIntegers) {
                val result = j + i + k
                if (result == 2020) {

                    val product = j * i * k

                    println("First number is $j")
                    println("Second number is $i")
                    println("Product is $product")

                }
            }
        }


    }

}