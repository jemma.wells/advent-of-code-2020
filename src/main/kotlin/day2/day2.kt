package day2

import java.io.File

fun main() {

    val lines = File("day2input.csv")
        .readLines()
        .map { it }

    var validPasswordCount = 0

    for (line in lines) {

        val splitForPassword = line.split(":")
        val splitForMinMax = line.split("-", " ")
        val splitForLetter = line.split(" ", ":")
        val password = splitForPassword[1].trimStart()
        val minimum = splitForMinMax[0].toInt()
        val maximum = splitForMinMax[1].toInt()
        val letter: String = splitForLetter[1]


        val chars = password.toList()
        var characterCount = 0


        for (char in chars) {

            if (char.toString() == letter) {

                characterCount += 1
            }
        }

        if (characterCount in minimum..maximum) {
            println("valid!")
            validPasswordCount += 1
            println(validPasswordCount)

        } else println("invalid")

    }
}