package day2

import java.io.File

fun main() {

    val lines = File("day2input.csv")
        .readLines()
        .map { it }

    var validPasswordCount = 0

    for (line in lines) {

        val splitForPassword = line.split(":")
        val splitForMinMax = line.split("-", " ")
        val splitForLetter = line.split(" ", ":")
        val password = splitForPassword[1].trimStart()
        val firstPosition = splitForMinMax[0].toInt()
        val secondPosition = splitForMinMax[1].toInt()
        val letter: String = splitForLetter[1]


        val chars = password.toList()


            if ((chars[firstPosition - 1].toString() == letter) xor (chars[secondPosition - 1].toString() == letter)) {
                validPasswordCount += 1
                println(validPasswordCount)
            }
    }
}
