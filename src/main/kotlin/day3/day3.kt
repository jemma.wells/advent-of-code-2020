package day3

import readPuzzleInput

fun main() {

    val lines = readPuzzleInput("day3input.csv")

    val x = 3
    var treeCount = 0

    for (i in lines.withIndex()) {

        val currentLine = i.value
        val lineLength = currentLine.length
        val position = ((i.index) * x) % lineLength
        val character = currentLine[position]


        if (character.toString() == "#") {
            treeCount += 1
        }
        println(treeCount)
    }
}