package day3

import java.io.File

fun main() {

    val lines = File("day3input.csv")
        .readLines()
        .toList()

    var treeCount = 0

    for (i in lines.withIndex()) {

        // This only works out the 2 down 1 across scenario - incomplete

        val currentLine = i.value
        val lineLength = currentLine.length

        if ((i.index) % 2 == 0 && i.index < lines.size) {

            val position = ((i.index) / 2) % lineLength
            val character = currentLine[position]


            if (character.toString() == "#") {
                treeCount += 1
            }
            println(treeCount)
        }
    }
}
