package day3

import java.io.File

    val lines = File("day3input.csv")
        .readLines()
        .toList()


data class Slope(val right: Int, val down: Int)

fun main() {
    println(solvePuzzlePart1(lines, 3, 1))
}

fun solvePuzzlePart1(input: List<String>, stepRight: Int, stepDown: Int): Int {
    val stepRightIterator = generateSequence(0) { it + stepRight }.iterator()

    return TobogganMap(input)
        .mapIndexedNotNull { index, row ->
            if (index % stepDown == 0)
                row[stepRightIterator.next()]
            else null
        }
        .count { it == Square.TREE }
}

class TobogganMap(private val input: List<String>) : Iterable<Row> {
    operator fun get(rowIndex: Int): Row = Row(input[rowIndex])

    override fun iterator(): Iterator<Row> {
        return input.map { Row(it) }.iterator()
    }
}

data class Row(private val row: String) {
    operator fun get(colIndex: Int) =
        if (row[colIndex % row.length] == '#') Square.TREE else Square.GAP
}

enum class Square {
    TREE, GAP
}
