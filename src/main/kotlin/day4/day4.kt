package day4

import readPuzzleInput

private val passportData = readPuzzleInput("day4input.csv")
private val expectedKeys = listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")
private var validPassportCount = 0
private val singlePassport = passportData.toString().trim().split(", ,")

fun main() {

    for (key in singlePassport) {

        val splitKeysOnly = key
            .trim()
            .split(":", " ", "[", "]")
            .filter {
                it in expectedKeys
            }

        if (splitKeysOnly.size == 7) {

            validPassportCount += 1
        }
    }
    println("$validPassportCount valid passports in list")
}
