package day4

import readPuzzleInputString

private val passportData = readPuzzleInputString("day4input.csv")
private val expectedKeys = listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")

fun main() {

    println(solvePart1(passportData))
//    println(parseInput(passportData))
}

fun parseInput(input: String): List<Map<String, String>> {
    return input.split("\n\n")
        .map { it ->
            it.split("[\n ]".toRegex())
                .mapNotNull {
                    if (it.isEmpty()) null else {
                        val split = it.split(":")
                        split[0] to split[1]
                    }
                }.toMap()
        }
}

fun solvePart1(input: String): Int = parseInput(input).count { validate(it) }

fun validate(passport: Map<String, String>) = passport.keys.containsAll(expectedKeys)