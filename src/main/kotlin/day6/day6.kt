package day6

import readPuzzleInput
import readPuzzleInputString

val answers = readPuzzleInput("day6input.csv")
val stringAnswers = readPuzzleInputString("day6input.csv")


fun main() {

    val groupOfAnswers = parseInput(stringAnswers)

    val numberOfDistinctAnswers = distinctAnswers(groupOfAnswers).sumBy { it.count() }
    println(numberOfDistinctAnswers)

}

fun parseInput(input: String): List<List<String>> {
    return input.trim().split("\n\n")
        .map { it ->
            it.split("\n")
        }
}

fun distinctAnswers(input: List<List<String>>): List<Set<Char>> {
    return input.map { it.joinToString("").toSet() }
}