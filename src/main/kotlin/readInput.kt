import java.io.File

fun readPuzzleInput(filename: String) = File(filename)
    .readLines()

fun readPuzzleInputString(filename: String) = File(filename)
    .readText()