package day6

import assertk.assertThat
import assertk.assertions.containsExactly
import org.junit.jupiter.api.Test

internal class Day6KtTest {
    @Test
    fun `should split input into list of lists`() {
        val stringInput = "abc\n" +
                "def\n" +
                "\n" +
                "fgh\n"

        val result = parseInput(stringInput)

        println(result)

        assertThat(result).containsExactly(listOf("abc", "def"), listOf("fgh"))
    }
}
